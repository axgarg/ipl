import urllib.request, json, time, re, MysqlUtils, Teams, Players, Matches, Deliveries
cb_match_id = 22398
match_id = None

def fetch_match(match_id):
    global scraped_result
    url = "https://www.cricbuzz.com/match-api/" + str(match_id) + "/commentary-full.json"
    req = urllib.request.Request(url, headers={'User-Agent' : "Magic Browser"}) 
    response = urllib.request.urlopen(req)
    jsonResp = json.load(response)
    scraped_result = jsonResp
    return jsonResp 


jsonR = fetch_match(cb_match_id)
# Teams.insert_teams(jsonR)
# Players.insert_players(jsonR)
# Matches.insert_match_sql(jsonR)

Deliveries.create_deliveries(jsonR)



# a, b, c, d, e, f = fetch_team_details(jsonR)
# print(e, f)
# create_match_dict(jsonR)

# Caught&Bowled!!
# Caught by














# insert_match_sql(jsonResp)



# create_deliveries(jsonResp)


# if int(ball_no) == 91 and int(inning) == 1:
    # runs_scored, extra, out = calculate_runs(comments_arr, False)
    # print(runs_scored, extra, out, comments_arr)


# CREATE TABLE deliveries (
# 	match_id DECIMAL(38, 0) NOT NULL, 
# 	inning DECIMAL(38, 0) NOT NULL, 
# 	batting_team VARCHAR(27) NOT NULL, 
# 	bowling_team VARCHAR(27) NOT NULL, 
# 	`over` DECIMAL(38, 0) NOT NULL, 
# 	ball DECIMAL(38, 0) NOT NULL, 
# 	batsman VARCHAR(20) NOT NULL, 
# 	non_striker VARCHAR(20) NOT NULL, 
# 	bowler VARCHAR(17) NOT NULL, 
# 	is_super_over BOOL NOT NULL, 
# 	wide_runs DECIMAL(38, 0) NOT NULL, 
# 	bye_runs DECIMAL(38, 0) NOT NULL, 
# 	legbye_runs DECIMAL(38, 0) NOT NULL, 
# 	noball_runs DECIMAL(38, 0) NOT NULL, 
# 	penalty_runs DECIMAL(38, 0) NOT NULL, 
# 	batsman_runs DECIMAL(38, 0) NOT NULL, 
# 	extra_runs DECIMAL(38, 0) NOT NULL, 
# 	total_runs DECIMAL(38, 0) NOT NULL, 
# 	player_dismissed VARCHAR(20), 
# 	dismissal_kind VARCHAR(21), 
# 	fielder VARCHAR(21), 
# 	CHECK (is_super_over IN (0, 1))
# );