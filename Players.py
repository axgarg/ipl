import MysqlUtils

def players_map(jsonResp):

    players_name_id = {}
    players_id_name = {}

    players = jsonResp['players']
    for player in players:
        player_id = player['id']
        player_name = player['name']
        players_id_name[player_id] = player_name
        players_name_id[player_name] = player_id

    return players_id_name, players_name_id 

def insert_players(jsonResp):
    players = jsonResp['players']
    for player in players:
        player_id = player['id']
        player_name = player['name']
        player_f_name = player['f_name']
        player_bat_style = player['bat_style'] if 'bat_style' in player else ""
        player_bowl_style = player['bowl_style'] if 'bowl_style' in player else "" 
        player_speciality = player['speciality']
        sqlstmt = "INSERT INTO Players (id, name, f_name, bat_style, bowl_style, speciality) VALUES (%s,%s,%s,%s,%s,%s)"
        sqlargs = (player_id, player_name, player_f_name, player_bat_style, player_bowl_style, player_speciality)
        MysqlUtils.insert_in_mysql(sqlstmt, sqlargs)