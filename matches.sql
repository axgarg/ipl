CREATE TABLE Matches (
	id DECIMAL(38, 0) NOT NULL UNIQUE,
	series_name VARCHAR(100) NOT NULL,
	series_sn VARCHAR(30) NOT NULL,
	category VARCHAR(30) NOT NULL,
	type VARCHAR(30) NOT NULL,
	season DECIMAL(38, 0) NOT NULL, 
	city VARCHAR(50), 
	country VARCHAR(50),
	venue VARCHAR(100),
	date VARCHAR(10) NOT NULL, 
	team1 VARCHAR(27) NOT NULL, 
	team2 VARCHAR(27) NOT NULL, 
	toss_winner VARCHAR(27) NOT NULL, 
	toss_winner_id DECIMAL(38, 0) NOT NULL, 
	toss_decision VARCHAR(30) NOT NULL, 
	result VARCHAR(9), 
	dl_applied BOOL NOT NULL, 
	winner VARCHAR(27), 
	winner_id DECIMAL(38, 0), 
	win_by_runs DECIMAL(38, 0) NOT NULL, 
	win_by_wickets DECIMAL(38, 0) NOT NULL, 
	player_of_match DECIMAL(38, 0) NOT NULL, 
	umpire1 VARCHAR(21), 
	umpire2 VARCHAR(21), 
	umpire3 VARCHAR(21),
	team1_squad VARCHAR(1000),
	team2_squad VARCHAR(1000)
);


CREATE TABLE Teams (
	id DECIMAL(38, 0) NOT NULL UNIQUE, 
	name VARCHAR(100) NOT NULL, 
	s_name VARCHAR(100) NOT NULL
);

CREATE TABLE Players (
	id DECIMAL(38, 0) NOT NULL UNIQUE, 
	name VARCHAR(100) NOT NULL, 
	f_name VARCHAR(100) NOT NULL, 
	bat_style VARCHAR(30) NOT NULL,
	bowl_style VARCHAR(30) NOT NULL,
	speciality VARCHAR(30) NOT NULL
);



