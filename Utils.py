def fetch_team_details(jsonResp):
    team1_id = jsonResp['team1']['id']
    team2_id = jsonResp['team2']['id']

    team_id_dict = {}
    team_id_dict[team1_id] = jsonResp['team1']['name']
    team_id_dict[team2_id] = jsonResp['team2']['name']

    team_name_dict = {}
    team_name_dict[jsonResp['team1']['name']] = team1_id
    team_name_dict[jsonResp['team2']['name']] = team2_id

    team1_squad = jsonResp['team1']['squad'] + jsonResp['team1']['squad_bench']
    team2_squad = jsonResp['team2']['squad'] + jsonResp['team2']['squad_bench']

    toss_winner = team_name_dict[jsonResp['toss']['winner']]
    toss_decision = jsonResp['toss']['decision']
    battng_1_innings = None 
    bowling_1_innings = None

    if team1_id == toss_winner:
        if toss_decision == "Fielding":
            battng_1_innings = team2_id
            bowling_1_innings = team1_id
        else:
            battng_1_innings = team1_id
            bowling_1_innings = team2_id

    elif team2_id == toss_winner:
        if toss_decision == "Fielding":
            battng_1_innings = team1_id
            bowling_1_innings = team2_id
        else:
            battng_1_innings = team2_id
            bowling_1_innings = team1_id

    return team_id_dict, team_name_dict, team1_squad, team2_squad, battng_1_innings, bowling_1_innings