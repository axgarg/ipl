import urllib.request, json, pymysql, time, re
conn = pymysql.connect(host='localhost', port=3306, user='root', passwd='Welcome1@LI', db='test')
cur = None

def insert_in_mysql(sqlstmt, sqlargs):
    global cur
    if cur is None:
        cur = conn.cursor()
    try:
        cur.execute(sqlstmt, sqlargs)
        conn.commit()
    except Exception as e:
        print("exception is", e)

def closeCursor():
    global cur
    if cur is not None:
        cur.close()
        cur = None

url = "https://www.cricbuzz.com/match-api/22398/commentary-full.json"

# CREATE TABLE deliveries (
# 	match_id DECIMAL(38, 0) NOT NULL, 
# 	inning DECIMAL(38, 0) NOT NULL, 
# 	batting_team VARCHAR(27) NOT NULL, 
# 	bowling_team VARCHAR(27) NOT NULL, 
# 	`over` DECIMAL(38, 0) NOT NULL, 
# 	ball DECIMAL(38, 0) NOT NULL, 
# 	batsman VARCHAR(20) NOT NULL, 
# 	non_striker VARCHAR(20) NOT NULL, 
# 	bowler VARCHAR(17) NOT NULL, 
# 	is_super_over BOOL NOT NULL, 
# 	wide_runs DECIMAL(38, 0) NOT NULL, 
# 	bye_runs DECIMAL(38, 0) NOT NULL, 
# 	legbye_runs DECIMAL(38, 0) NOT NULL, 
# 	noball_runs DECIMAL(38, 0) NOT NULL, 
# 	penalty_runs DECIMAL(38, 0) NOT NULL, 
# 	batsman_runs DECIMAL(38, 0) NOT NULL, 
# 	extra_runs DECIMAL(38, 0) NOT NULL, 
# 	total_runs DECIMAL(38, 0) NOT NULL, 
# 	player_dismissed VARCHAR(20), 
# 	dismissal_kind VARCHAR(21), 
# 	fielder VARCHAR(21), 
# 	CHECK (is_super_over IN (0, 1))
# );


req = urllib.request.Request(url, headers={'User-Agent' : "Magic Browser"}) 
response = urllib.request.urlopen(req)
jsonResp = json.load(response)
comment_lines = jsonResp['comm_lines']
startInx = 0
endIdx = len(comment_lines)


# match statistics

# team id map
team_id_dict = {}
match_id = jsonResp['id']
team_id_dict[jsonResp['team1']['id']] = jsonResp['team1']['name']
team_id_dict[jsonResp['team2']['id']] = jsonResp['team2']['name']

team_name_dict = {}
team_name_dict[jsonResp['team1']['name']] = jsonResp['team1']['id']
team_name_dict[jsonResp['team2']['name']] = jsonResp['team2']['id']



# players map

def insert_players(jsonResp):

    players = jsonResp['players']

    for player in players:
        player_id = player['id']
        player_name = player['name']
        player_f_name = player['f_name']
        player_bat_style = player['bat_style'] if 'bat_style' in player else ""
        player_bowl_style = player['bowl_style'] if 'bowl_style' in player else "" 
        player_speciality = player['speciality']
        sqlstmt = "INSERT INTO Players (id, name, f_name, bat_style, bowl_style, speciality) VALUES (%s,%s,%s,%s,%s,%s)"
        sqlargs = (player_id, player_name, player_f_name, player_bat_style, player_bowl_style, player_speciality)
        insert_in_mysql(sqlstmt, sqlargs)


def insertTeams(jsonResp):

    team1_id = jsonResp['team1']['id']
    team1_name = jsonResp['team1']['name']
    team1_name_sn = jsonResp['team1']['s_name']
    sqlstmt_team1 = "INSERT INTO Teams (ID, name, s_name) VALUES (%s,%s,%s)"
    sqlstmt_team1_args = (team1_id, team1_name, team1_name_sn) 
    insert_in_mysql(sqlstmt_team1, sqlstmt_team1_args)

    team2_name = jsonResp['team2']['name']
    team2_name_sn = jsonResp['team2']['s_name']
    team2_id = jsonResp['team2']['id']
    sqlstmt_team2 = "INSERT INTO Teams (ID, name, s_name) VALUES (%s,%s,%s)"
    sqlstmt_team2_args = (team2_id, team2_name, team2_name_sn)
    insert_in_mysql(sqlstmt_team2, sqlstmt_team2_args)


# CREATE TABLE Matches (
# 	id DECIMAL(38, 0) NOT NULL, 
# 	series_name VARCHAR(100), 
# 	series_sn VARCHAR(30), 
# 	category VARCHAR(30), 
# 	type VARCHAR(30), 
# 	season DECIMAL(38, 0) NOT NULL, 
# 	city VARCHAR(30), 
# 	country VARCHAR(30), 
# 	venue VARCHAR(52) NOT NULL, 
# 	date VARCHAR(30) NOT NULL, 
# 	team1 VARCHAR(27) NOT NULL, 
# 	team2 VARCHAR(27) NOT NULL, 
# 	toss_winner VARCHAR(50) NOT NULL, 
# 	toss_winner_id DECIMAL(38, 0) NOT NULL, 
# 	toss_decision VARCHAR(30) NOT NULL, 
# 	result VARCHAR(9) NOT NULL, 
# 	dl_applied BOOL NOT NULL, 
# 	winner VARCHAR(50), 
# 	winner_id DECIMAL(38, 0) NOT NULL, 
# 	win_by_runs DECIMAL(38, 0) NOT NULL, 
# 	win_by_wickets DECIMAL(38, 0) NOT NULL, 
# 	player_of_match VARCHAR(17), 
# 	umpire1 VARCHAR(21), 
# 	umpire2 VARCHAR(21), 
# 	umpire3 VARCHAR(21)
# );

def create_match_dict(jsonResp):
    match_dict = {}
    match_dict['id'] = jsonResp['id']
    match_dict['series_name'] = jsonResp['series']['name']
    match_dict['series_sn'] = jsonResp['series']['short_name']
    match_dict['category'] = jsonResp['series']['category']
    match_dict['type'] = jsonResp['type']
    match_dict['season'] = 0
    match_dict['city'] = jsonResp['venue']['city']
    match_dict['country'] = jsonResp['venue']['country']
    match_dict['venue'] = jsonResp['venue']['name']
    match_dict['date'] = time.strftime('%Y-%m-%d', time.localtime(1547108100))
    match_dict['team1'] = jsonResp['team1']['id']
    match_dict['team2'] = jsonResp['team2']['id']
    match_dict['toss_winner'] = jsonResp['toss']['winner']
    match_dict['toss_winner_id'] = team_name_dict[jsonResp['toss']['winner']]
    match_dict['toss_decision'] = jsonResp['toss']['decision']
    match_dict['result'] = ''
    match_dict['dl_applied'] = ''
    match_dict['winner'] = team_id_dict[str(jsonResp['winning_team_id'])]
    match_dict['winner_id'] = jsonResp['winning_team_id']
    temp_result = jsonResp['winningmargin'].split(" ")
    match_dict['win_by_runs'] = int(temp_result[0]) if temp_result[1].strip() == "Runs" else 0
    match_dict['win_by_wickets'] = int(temp_result[0]) if temp_result[1].strip() == "Wickets" else 0
    match_dict['player_of_match'] = jsonResp['mom'][0]
    match_dict['umpire1'] = ''
    match_dict['umpire2'] = ''
    match_dict['umpire3'] = ''
    return match_dict



def calculate_runs(comments_arr, recurse, run_type = "strike"):

    isExtra = False
    if recurse == False:
        comments_str =  comments_arr[1].strip()
        comments_str =  comments_str.lower()
    else:
        isExtra = True
        comments_str =  comments_arr[2].strip()
        comments_str =  comments_str.lower()

    if comments_str.find('out') > -1:
        return 0, isExtra, True, run_type 
    elif comments_str.find('1 run') > -1:
        return 1, isExtra, False, run_type
    elif comments_str.find('2 runs') > -1:
        return 2, isExtra, False, run_type
    elif comments_str.find('3 runs') > -1:
        return 3, isExtra, False, run_type
    elif comments_str.find('no run') > -1:
        return 0, isExtra, False, run_type
    elif comments_str.find('byes') > -1:
        return calculate_runs(comments_arr, True, 'byes')
    elif comments_str.find('four') > -1:
        return 4, isExtra, False, run_type
    elif comments_str.find('six') > -1:
        return 6, isExtra, False, run_type
    elif comments_str.find('wide') > -1:
        # print("wide ---->> ", comments_str)
        if comments_str.find('wides'):
            try:
                r = int(re.sub("\D", "", comments_str))
                return r, True, False, 'wide'
            except Exception:
                return 1, True, False, 'wide'
        else:
            return 1, True, False, 'wide'
    elif comments_str.find('no ball') > -1:
        return calculate_runs(comments_arr, True, 'no ball')
    else:
        try:
                r = int(re.sub("\D", "", comments_str))
                return r, True, False, run_type
        except Exception:
                print("can't find anything", comments_arr)
                return -1, False, False, 'miscellanoeous'



def insert_match_sql(jsonResp):
    match_props = create_match_dict(jsonResp)
    columns = ", ".join(match_props.keys())
    empty_values = ()
    for i in match_props:
        empty_values = empty_values + ("%s",)
    qry = "Insert Into Matches (%s) Values (%s)" % (columns, empty_values)
    insert_in_mysql(qry, tuple(match_props.values()))


# insert_match_sql(jsonResp)

def create_deliveries(jsonResp):
    
    global endIdx, match_id
    comment_lines = jsonResp['comm_lines']

    i = endIdx
    for i in range(endIdx, 0, -1):
        data = {}
        try: 
            temp = comment_lines[i]
            if 'b_no' not in temp:
                continue
            inning = temp['i_id']
            over_no = temp['o_no']
            ball_no = temp['b_no']
            batsman_id = temp['batsman'][0]['id']
            bowler_id = temp['bowler'][0]['id']
            comments = temp['comm']
            comments_arr = comments.split(',')
            runs_scored, extra, out, run_type = calculate_runs(comments_arr, False)
            data['match_id'] = match_id
            data['inning'] = inning
            data['batting_team'] = None
            data['bowling_team'] = None
            data['over'] = over_no
            data['ball'] = ball_no
            data['batsman'] = batsman_id
            data['non_striker'] = None
            data['bowler'] = bowler_id
            data['is_super_over'] = False
            data['wide_runs'] = True if run_type == 'wide' else False
            data['legbye_runs'] = True if run_type == 'byes' else False
            data['noball_runs'] = True if run_type == 'no ball' else False
            data['penalty_runs'] = 0
            data['batsman_runs'] = runs_scored if run_type == 'strike' else 0
            data['extra_runs'] = runs_scored if extra == True else 0
            data['total_runs'] = None
            data['player_dismissed'] = None
            data['dismissal_kind'] = None
            data['fielder'] = None
            data['check'] = None
            print(inning, ball_no, over_no, runs_scored, extra, out, run_type)

        except Exception as e:
            x = 1

create_deliveries(jsonResp)


# if int(ball_no) == 91 and int(inning) == 1:
    # runs_scored, extra, out = calculate_runs(comments_arr, False)
    # print(runs_scored, extra, out, comments_arr)


# CREATE TABLE deliveries (
# 	match_id DECIMAL(38, 0) NOT NULL, 
# 	inning DECIMAL(38, 0) NOT NULL, 
# 	batting_team VARCHAR(27) NOT NULL, 
# 	bowling_team VARCHAR(27) NOT NULL, 
# 	`over` DECIMAL(38, 0) NOT NULL, 
# 	ball DECIMAL(38, 0) NOT NULL, 
# 	batsman VARCHAR(20) NOT NULL, 
# 	non_striker VARCHAR(20) NOT NULL, 
# 	bowler VARCHAR(17) NOT NULL, 
# 	is_super_over BOOL NOT NULL, 
# 	wide_runs DECIMAL(38, 0) NOT NULL, 
# 	bye_runs DECIMAL(38, 0) NOT NULL, 
# 	legbye_runs DECIMAL(38, 0) NOT NULL, 
# 	noball_runs DECIMAL(38, 0) NOT NULL, 
# 	penalty_runs DECIMAL(38, 0) NOT NULL, 
# 	batsman_runs DECIMAL(38, 0) NOT NULL, 
# 	extra_runs DECIMAL(38, 0) NOT NULL, 
# 	total_runs DECIMAL(38, 0) NOT NULL, 
# 	player_dismissed VARCHAR(20), 
# 	dismissal_kind VARCHAR(21), 
# 	fielder VARCHAR(21), 
# 	CHECK (is_super_over IN (0, 1))
# );