import MysqlUtils

def insert_teams(jsonResp):
    team1_id = jsonResp['team1']['id']
    team1_name = jsonResp['team1']['name']
    team1_name_sn = jsonResp['team1']['s_name']
    sqlstmt_team1 = "INSERT INTO Teams (ID, name, s_name) VALUES (%s,%s,%s)"
    sqlstmt_team1_args = (team1_id, team1_name, team1_name_sn) 
    MysqlUtils.insert_in_mysql(sqlstmt_team1, sqlstmt_team1_args)

    team2_name = jsonResp['team2']['name']
    team2_name_sn = jsonResp['team2']['s_name']
    team2_id = jsonResp['team2']['id']
    sqlstmt_team2 = "INSERT INTO Teams (ID, name, s_name) VALUES (%s,%s,%s)"
    sqlstmt_team2_args = (team2_id, team2_name, team2_name_sn)
    MysqlUtils.insert_in_mysql(sqlstmt_team2, sqlstmt_team2_args)