import pymysql
conn = pymysql.connect(host='localhost', port=3306, user='root', passwd='Welcome1@LI', db='test')
cur = None

def insert_in_mysql(sqlstmt, sqlargs = None):
    global cur
    if cur is None:
        cur = conn.cursor()
    try:
        if sqlargs is not None:
            cur.execute(sqlstmt, sqlargs)
        else:
            cur.execute(sqlstmt)
        conn.commit()
    except Exception as e:
        print("exception is", e)

def closeCursor():
    global cur
    if cur is not None:
        cur.close()
        cur = None