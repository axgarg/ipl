import MysqlUtils, Utils, time

def create_match_dict(jsonResp):
    match_id = jsonResp['id']
    team_id_map, team_name_map, team1_squad, team2_squad, _, _ = Utils.fetch_team_details(jsonResp)
    match_dict = {}
    match_dict['id'] = match_id
    match_dict['series_name'] = jsonResp['series']['name']
    match_dict['series_sn'] = jsonResp['series']['short_name']
    match_dict['category'] = jsonResp['series']['category']
    match_dict['type'] = jsonResp['type']
    match_dict['season'] = 0
    match_dict['city'] = jsonResp['venue']['city']
    match_dict['country'] = jsonResp['venue']['country']
    match_dict['venue'] = jsonResp['venue']['name']
    match_dict['date'] = time.strftime('%Y-%m-%d', time.localtime(1547108100))
    match_dict['team1'] = jsonResp['team1']['id']
    match_dict['team2'] = jsonResp['team2']['id']
    match_dict['toss_winner'] = jsonResp['toss']['winner']
    match_dict['toss_winner_id'] = team_name_map[jsonResp['toss']['winner']]
    match_dict['toss_decision'] = jsonResp['toss']['decision']
    match_dict['result'] = ''
    match_dict['dl_applied'] = 0
    match_dict['winner'] = team_id_map[str(jsonResp['winning_team_id'])]
    match_dict['winner_id'] = jsonResp['winning_team_id']
    temp_result = jsonResp['winningmargin'].split(" ")
    match_dict['win_by_runs'] = int(temp_result[0]) if temp_result[1].strip() == "Runs" else 0
    match_dict['win_by_wickets'] = int(temp_result[0]) if temp_result[1].strip() == "Wickets" else 0
    match_dict['player_of_match'] = jsonResp['mom'][0]
    match_dict['umpire1'] = ''
    match_dict['umpire2'] = ''
    match_dict['umpire3'] = ''
    match_dict['team1_squad'] = str(team1_squad)
    match_dict['team2_squad'] = str(team2_squad)
    return match_dict

def insert_match_sql(jsonResp):
    data = create_match_dict(jsonResp)
    cols = data.keys()
    vals = data.values()
    sql = "INSERT INTO %s (%s) VALUES (%s)" % ('Matches', ",".join(cols), ",".join("'" + str(x) + "'" for x in vals))
    print(sql)
    try:
        MysqlUtils.insert_in_mysql(sql)
    except Exception as e:
        print("mysql exception------------>>>>", e)