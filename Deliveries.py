import Utils, re, Players

def calculate_runs(comments_arr, recurse, run_type = "strike"):

    isExtra = False
    if recurse == False:
        comments_str =  comments_arr[1].strip()
        comments_str =  comments_str.lower()
    else:
        isExtra = True
        comments_str =  comments_arr[2].strip()
        comments_str =  comments_str.lower()

    if comments_str.find('out') > -1:
        # print(comments_arr)
        return 0, isExtra, True, run_type 
    elif comments_str.find('1 run') > -1:
        return 1, isExtra, False, run_type
    elif comments_str.find('2 runs') > -1:
        return 2, isExtra, False, run_type
    elif comments_str.find('3 runs') > -1:
        return 3, isExtra, False, run_type
    elif comments_str.find('no run') > -1:
        return 0, isExtra, False, run_type
    elif comments_str.find('byes') > -1:
        return calculate_runs(comments_arr, True, 'byes')
    elif comments_str.find('four') > -1:
        return 4, isExtra, False, run_type
    elif comments_str.find('six') > -1:
        return 6, isExtra, False, run_type
    elif comments_str.find('wide') > -1:
        if comments_str.find('wides'):
            try:
                r = int(re.sub("\D", "", comments_str))
                return r, True, False, 'wide'
            except Exception:
                return 1, True, False, 'wide'
        else:
            return 1, True, False, 'wide'
    elif comments_str.find('no ball') > -1:
        return calculate_runs(comments_arr, True, 'no ball')
    else:
        try:
                r = int(re.sub("\D", "", comments_str))
                return r, True, False, run_type
        except Exception:
                print("can't find anything", comments_arr)
                return -1, False, False, 'miscellanoeous'


def findPlayers(comments_arr, player_name_id):
    print(comments_arr[len(comments_arr)-1])
    comments_0 = comments_arr[0].split("to")
    res = []
    for p in comments_0:
        p = p.strip().lower()
        l = [key for key, value in player_name_id.items() if p in key.lower()]
        if len(l) == 1:
            res.append(player_name_id[l[0]])
    return res

def create_deliveries(jsonResp):

    match_id = jsonResp['id']
    comment_lines = jsonResp['comm_lines']
    endIdx = len(comment_lines)
    _,_,_,_, batting1, bowling1 = Utils.fetch_team_details(jsonResp)

    i = endIdx
    for i in range(endIdx, 0, -1):
        data = {}
        try: 
            temp = comment_lines[i]
            if 'b_no' not in temp:
                continue
            inning = temp['i_id']
            over_no = temp['o_no']
            ball_no = temp['b_no']
            batsman_id = temp['batsman'][0]['id']
            bowler_id = temp['bowler'][0]['id']
            comments = temp['comm']
            comments_arr = comments.split(',')
            runs_scored, extra, out, run_type = calculate_runs(comments_arr, False)
            player_dismissed = None
            dismissal_kind = None
            fielder = None

            if out == True:
                _, player_name_id = Players.players_map(jsonResp)
                fielder = findPlayers(comments_arr, player_name_id)

            data['match_id'] = match_id
            data['inning'] = inning
            data['batting_team'] = batting1 if int(inning) == 1 else bowling1
            data['bowling_team'] = bowling1 if int(inning) == 1 else batting1
            data['over'] = over_no
            data['ball'] = ball_no
            data['batsman'] = batsman_id
            data['non_striker'] = None
            data['bowler'] = bowler_id
            data['is_super_over'] = False
            data['wide_runs'] = True if run_type == 'wide' else False
            data['legbye_runs'] = True if run_type == 'byes' else False
            data['noball_runs'] = True if run_type == 'no ball' else False
            data['penalty_runs'] = 0
            data['batsman_runs'] = runs_scored if run_type == 'strike' else 0
            data['extra_runs'] = runs_scored if extra == True else 0
            data['total_runs'] = None
            data['player_dismissed'] = player_dismissed
            data['dismissal_kind'] = dismissal_kind
            data['fielder'] = fielder
            data['check'] = None
            # print(inning, ball_no, over_no, runs_scored, extra, out, run_type)

        except Exception as e:
            # print(e)
            x = 1
    